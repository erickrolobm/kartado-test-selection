import * as React from "react";
import jsonServerProvider from 'ra-data-json-server';
import { Admin, Resource } from 'react-admin';
import Dashboard from "./Dashboard/Home";
import authProvider from "./Provider/Auth";

//Resources's form of display
import { UserCreate, UserEdit, UserList, UserShowContent } from "./Lists/Users";
import { PostList, PostEdit, PostCreate, PostShow } from "./Lists/Posts"
import { AlbumCreate, AlbumEdit, AlbumList, AlbumShow  } from "./Lists/Album";
import { TodoCreate, TodoEdit, TodosList } from "./Lists/Todos";
import { PhotoCreate, PhotoEdit, PhotoList, PhotoShow } from "./Lists/Photos";

//icons
import PostIcon from '@mui/icons-material/Book';
import UserIcon from '@mui/icons-material/Group';
import PhotoIcon from '@mui/icons-material/Photo';
import AlbumIcon from '@mui/icons-material/PhotoAlbum';



const dataProvider = jsonServerProvider('https://jsonplaceholder.typicode.com');

const App = () => (
  <Admin authProvider={authProvider} dashboard={Dashboard} dataProvider={dataProvider} >
    <Resource 
      name="users" 
      list={UserList} 
      show={UserShowContent} 
      edit={UserEdit}
      create={UserCreate}
      icon={UserIcon}
    />
    <Resource 
      name="photos" 
      list={PhotoList} 
      edit={PhotoEdit}
      create={PhotoCreate}
      show={PhotoShow} 
      icon={PhotoIcon}
    />
    <Resource 
      name="albums" 
      list={AlbumList} 
      edit={AlbumEdit}
      create={AlbumCreate}
      show={AlbumShow} 
      icon={AlbumIcon}
    />
    <Resource 
      name="posts" 
      list={PostList} 
      create={PostCreate} 
      edit={PostEdit} 
      show={PostShow} 
      icon={PostIcon}
    />
    <Resource 
      name="todos" 
      edit={TodoEdit}
      create={TodoCreate}
      list={TodosList} 
    />
  </Admin>
);

export default App;
