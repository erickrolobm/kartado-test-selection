import * as React from "react";
import { useRecordContext } from "react-admin";
import { Typography } from '@mui/material';

//icons
import CircleIcon from '@mui/icons-material/Circle';


const StatusField = ({source}) => {
    const record = useRecordContext();
    return record ? (
        <Typography>
            {record[source]}
        <CircleIcon sx={{color:"gray", height:"0.5em", paddingLeft:"1"}}/>
        </Typography>
    ):null;
}

export default StatusField;