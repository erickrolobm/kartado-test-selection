import * as React from "react";
import { useRecordContext } from "react-admin";


const FullImage = ({source}) => {
    const record = useRecordContext();
    return record ? (
        <img src={record[source]} alt={record[source.title]} style={{maxHeight:"300px", maxWidth:"300px", minHeight:"150px", minWidth:"150px", margin:"0 auto 5px auto"}}/>
    ):null;
}

export default FullImage;