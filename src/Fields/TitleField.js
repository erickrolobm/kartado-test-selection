import * as React from "react";
import { useRecordContext } from "react-admin";
import { Typography } from '@mui/material';


const TitleField = ({source}) => {
    const record = useRecordContext();
    return record ? (
        <Typography sx={{fontWeight: "700", fontSize: "1.2em"}}>
            {record[source]}
        </Typography>
    ):null;
}

export default TitleField;