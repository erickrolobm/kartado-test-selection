import * as React from "react";
import { 
    Datagrid, 
    EmailField, 
    BooleanField,
    List, 
    Edit, 
    SimpleForm,
    TextInput,
    TextField, 
    EditButton, 
    Show, 
    TabbedShowLayout, 
    SimpleShowLayout,
    Tab, 
    ReferenceManyField,
    SimpleList,
    Create
} from 'react-admin';
import { Grid } from '@mui/material';

//icons
import DoneIcon from '@mui/icons-material/Done';
import ClearIcon from '@mui/icons-material/Clear';

//custom react admin
import TitleField from "../Fields/TitleField";
import Url from "../Fields/Url";

//expand
import { ExpandAlbum } from "./Album";

const UsersTitle = () => {    
    return <span>User History Page</span>
}

export const UserList = () => (
    <List>
        <Datagrid rowClick="show">
            <TextField source="username" />
            <TextField source="name" />
            <TextField source="company.name" />
            <EditButton />
        </Datagrid>
    </List>
);

export const UserStatusList = () =>(
    <List>
        <Datagrid rowClick="edit">
            <TextField source="name" />
        </Datagrid>
    </List>
);

export const UserEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput disabled source="id" />
            <TextInput source="name" />
            <TextInput source="username" />
            <TextInput source="email" />
            <TextInput source="address.street" />
            <TextInput source="phone" />
            <TextInput source="website" />
            <TextInput source="company.name" />
        </SimpleForm>
    </Edit>
);

export const UserCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <TextInput source="username" />
            <TextInput source="email" />
            <TextInput source="address.street" />
            <TextInput source="phone" />
            <TextInput source="website" />
            <TextInput source="company.name" />
        </SimpleForm>
    </Create>
);

export const UserShowContent = () => (
    <Show title={<UsersTitle/>}>
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <SimpleShowLayout>
                    <TextField 
                        source="username" 
                        label="" 
                        sx={{fontSize:"1.2em", fontWeight: "700"}} 
                    />
                </SimpleShowLayout>
            </Grid>
            <Grid item xs={6}>
                <SimpleShowLayout>
                    <TextField source="name" />
                </SimpleShowLayout>
            </Grid>
            <Grid item xs={6}>
                <SimpleShowLayout>
                    <EmailField source="email" />
                </SimpleShowLayout>
            </Grid>
            <Grid item xs={6}>
                <SimpleShowLayout>
                    <TextField source="address.street" />
                    <TextField source="phone" />
                </SimpleShowLayout>
            </Grid>
            <Grid item xs={6}>
                <SimpleShowLayout>
                    <Url source="website" />
                    <TextField source="company.name" />
                </SimpleShowLayout>
            </Grid>
        </Grid>
        <TabbedShowLayout>
            <Tab label="posts">
                <ReferenceManyField label="" reference="posts" target="userId">
                    <SimpleList 
                        primaryText={record => <TitleField source="title"/>} 
                        secondaryText={record => <TextField source="body"/>}
                        linkType="show"
                    />
                </ReferenceManyField>
            </Tab>
            <Tab label="Albums" >
                <ReferenceManyField label="" reference="albums" target="userId">
                    <Datagrid expand={<ExpandAlbum/>} expandSingle rowClick="expand">
                        <TextField source="id" />
                        <TextField source="title" />
                    </Datagrid>
                </ReferenceManyField>
            </Tab>
            <Tab label="To dos">
                <ReferenceManyField label="" reference="todos" target="userId">
                <Datagrid>
                    <TextField source="id" />
                    <TextField source="title" />
                    <BooleanField 
                        source="completed" 
                        TrueIcon={record => <DoneIcon sx={{color: "green"}}/>} 
                        FalseIcon={record => <ClearIcon sx={{color: "red"}}/>} 
                    />
                </Datagrid>
                </ReferenceManyField>
            </Tab>
        </TabbedShowLayout>
    </Show>
);
