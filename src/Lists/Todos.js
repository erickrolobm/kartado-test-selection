import * as React from "react";
import { 
    List, 
    Edit, 
    Create, 
    SimpleForm,
    BooleanInput,
    Datagrid, 
    TextField, 
    BooleanField, 
    ReferenceField, 
    SelectInput, 
    TextInput, 
    ReferenceInput, 
    NullableBooleanInput, 
    EditButton 
} from "react-admin";

//icons
import DoneIcon from '@mui/icons-material/Done';
import ClearIcon from '@mui/icons-material/Clear';

const todosFilters = [
    <TextInput source="q" label="Search" alwaysOn />,
    <ReferenceInput source="userId" label="User" reference="users">
        <SelectInput optionText="username"/>
    </ReferenceInput>,
    <NullableBooleanInput alwaysOn source="completed"/>
];

export const TodosList = () => (
    <List filters={todosFilters}>
        <Datagrid>
            <TextField source="id" />
            <ReferenceField source="userId" reference="users" link="show">
                <TextField source="username"/>
            </ReferenceField>
            <TextField source="title" />
            <BooleanField 
                source="completed" 
                TrueIcon={record => <DoneIcon sx={{color: "green"}}/>} 
                FalseIcon={record => <ClearIcon sx={{color: "red"}}/>}
            />
            <EditButton/>
        </Datagrid>
    </List>
);

export const TodoEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <ReferenceInput source="userId" reference="users">
                <SelectInput optionText="username" />
            </ReferenceInput>
            <TextInput source="title" />
            <BooleanInput source="completed" />
        </SimpleForm>
    </Edit>
);

export const TodoCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <ReferenceInput source="userId" reference="users">
                <SelectInput optionText="username" />
            </ReferenceInput>
            <TextInput source="title" />
            <BooleanInput source="completed"/>
        </SimpleForm>
    </Create>
);