import * as React from "react";
import {useMediaQuery} from '@mui/material';
import { 
    Datagrid, 
    List, 
    Edit, 
    Create, 
    Show,
    SimpleShowLayout,
    ReferenceField, 
    TextField, 
    ImageField,
    SimpleList,
    SimpleForm,
    TextInput, 
    ReferenceInput,
    SelectInput,
    FileInput
} from 'react-admin';

//custom field
import FullImage from "../Fields/FullImage";

const photoFilters =[
    <TextInput source="q" label="Search" alwaysOn />,
    <ReferenceInput source="userId" label="User" reference="users">
        <SelectInput optionText="username"/>
    </ReferenceInput>
];

export const PhotoList = () => {
    const isSmall = useMediaQuery(theme => theme.breakpoints.down('sm'));

    return (
        <List filters={photoFilters}>
            {isSmall ? (
                <SimpleList 
                    primaryText={<ImageField source="thumbnailUrl"/>}
                    secondaryText={record => record.title}
                    tertiaryText={
                    <ReferenceField source="albumId" reference="albums">
                        <TextField source="title" />
                    </ReferenceField>}
                />
            ):(
            <Datagrid rowClick="show">
                <TextField source="id" />
                <ImageField label="Preview" source="thumbnailUrl" />
                <TextField source="title" />
                <ReferenceField source="albumId" reference="albums" link="show">
                    <TextField source="title" />
                </ReferenceField>
            </Datagrid>
            )}
        </List>    
)};

export const PhotoShow = () => (
    <Show>
        <SimpleShowLayout>
            <TextField source="title" />
            <ReferenceField source="albumId" reference="albums">
                <TextField label="Album" source="title"/>
            </ReferenceField>
            <FullImage source="url" />
        </SimpleShowLayout>
    </Show>
);

export const PhotoEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput disabled source="id" />
            <ReferenceInput source="albumId" reference="albums">
                <SelectInput optionText="title" />
            </ReferenceInput>
            <TextInput source="title" />
            <TextInput source="url" />
            <TextInput source="thumbnailUrl" />
        </SimpleForm>
    </Edit>
);

export const PhotoCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <TextInput disabled source="id" />
            <ReferenceInput source="albumId" reference="albums">
                <SelectInput optionText="title" />
            </ReferenceInput>
            <TextInput source="title" />
            <FileInput source="url" />
            <FileInput source="thumbnailUrl" />
        </SimpleForm>
    </Create>
);