import * as React from "react";
import {useMediaQuery} from '@mui/material';
import { 
    Datagrid, 
    List, 
    Edit, 
    SimpleForm,
    ReferenceField, 
    ReferenceManyField,
    TextField, 
    ShowButton, 
    Show, 
    SimpleShowLayout, 
    SingleFieldList,
    ImageField,
    TextInput,
    ReferenceInput,
    SelectInput,
    Create
} from 'react-admin';

//custom field
import FullImage from "../Fields/FullImage";

const albumFilters = [
    <TextInput source="q" label="Search" alwaysOn />,
    <ReferenceInput source="userId" label="User" reference="users">
        <SelectInput optionText="username"/>
    </ReferenceInput>
];

export const AlbumList = () => (
    <List filters={albumFilters}>
        <Datagrid expand={ExpandAlbum} expandSingle rowClick="expand">
            <TextField source="id" />
            <ReferenceField source="userId" reference="users">
                <TextField source="username" />
            </ReferenceField>
            <TextField source="title" />
            <ShowButton />
        </Datagrid>
    </List>
);

export const AlbumShow = () => {
    const isSmall = useMediaQuery(theme => theme.breakpoints.down('sm'));

    return(
    <Show>
        <SimpleShowLayout>
        <TextField source="title" label="" sx={{fontSize:"1.2em", fontWeight: "700"}} />
            <ReferenceField source="userId" reference="users" label="By" link="show">
                <TextField source="username" />
            </ReferenceField>
        </SimpleShowLayout>
        {isSmall ? (
        <ReferenceManyField perPage={50} label="photos" reference="photos" target="albumId">
            <SingleFieldList linkType="">
                <FullImage source="url" title="title" sx={{margin: "0px auto 5px auto"}}/>
            </SingleFieldList>
        </ReferenceManyField> 
        ):(
        <ReferenceManyField perPage={50} label="photos" reference="photos" target="albumId">
            <SingleFieldList linkType="" sx={{padding:"0 0 0 16px"}}>
                <FullImage source="url" title="title" sx={{width:"600px", height:"600px", objectFit:""}}/>
            </SingleFieldList>
        </ReferenceManyField> 
        )}
    </Show>)

};

export const AlbumEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <ReferenceInput source="userId" reference="users">
                <SelectInput optionText="username" />
            </ReferenceInput>
            <TextInput source="title" />
        </SimpleForm>
    </Edit>
);

export const AlbumCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <ReferenceInput source="userId" reference="users">
                <SelectInput optionText="username" />
            </ReferenceInput>
            <TextInput source="title" />
        </SimpleForm>
    </Create>
);

export const ExpandAlbum = () => (
    <ReferenceManyField perPage={50} label="photos" reference="photos" target="albumId">
        <SingleFieldList linkType="">
            <ImageField title="title" source="thumbnailUrl"/>
        </SingleFieldList>
    </ReferenceManyField> 
);