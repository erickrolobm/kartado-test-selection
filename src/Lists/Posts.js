import * as React from "react";
import {useMediaQuery} from '@mui/material';
import { 
    Edit,
    ReferenceInput,
    SelectInput,
    SimpleForm,
    SimpleList,
    SimpleShowLayout,
    Datagrid,
    EditButton,
    Create,
    List,
    Show,
    ReferenceField,
    TextField,
    TextInput, 
    useRecordContext,
    ShowButton
} from 'react-admin';

const PostTitle = () => {
    const record = useRecordContext();
    return <span>Post {record ? `"${record.title}"`: ""}</span>
}

const postFilters =[
    <TextInput source="q" label="Search" alwaysOn />,
    <ReferenceInput source="userId" label="User" reference="users">
        <SelectInput optionText="username"/>
    </ReferenceInput>
];

export const PostList = () => {
    const isSmall = useMediaQuery(theme => theme.breakpoints.down('sm'));

    return (
    <List filters={postFilters}>
        {isSmall ? (
            <SimpleList 
                primaryText={record => record.title}
                secondaryText = { record => (           
                    <ReferenceField source="userId" reference="users">
                        <TextField source="username" />
                    </ReferenceField>
                    )}
                linkType="show"    
            />

        ):( 
            <Datagrid>
                <TextField source="id"/>
                <ReferenceField source="userId" reference="users" link="show">
                    <TextField source="username" />
                </ReferenceField>
                <TextField source="title"/>
                <TextField source="body"/>
                <ShowButton/>
                <EditButton />
            </Datagrid>
        )}
    </List>

    );

};

export const PostEdit = props => (
    <Edit {...props} title={<PostTitle/>}>
        <SimpleForm>
            <TextInput disabled source="id" />
            <ReferenceInput source="userId" reference="users">
                <SelectInput optionText="username" />
            </ReferenceInput>
            <TextInput source="title" />
            <TextInput multiline source="body" />
        </SimpleForm>
    </Edit>
);

export const PostCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <ReferenceInput source="userId" reference="users">
                <SelectInput optionText="username" />
            </ReferenceInput>
            <TextInput source="title" />
            <TextInput multiline source="body" />
        </SimpleForm>
    </Create>
);

export const PostShow = () => (
    <Show>
        <SimpleShowLayout>
            <TextField 
                source="title" 
                label="" 
                sx={{fontSize:"1.2em", fontWeight: "700"}}  
            />
            <ReferenceField label="by" source="userId" reference="users">
                <TextField source="username" />
            </ReferenceField>
            <TextField source="body" />
        </SimpleShowLayout>
    </Show>
);