import * as React from "react";
import { Card, CardContent, CardHeader, Grid } from '@mui/material';
import { List, SimpleList, ReferenceField, TextField, Datagrid, BooleanField, Title } from "react-admin";

//icons
import DoneIcon from '@mui/icons-material/Done';
import ClearIcon from '@mui/icons-material/Clear';

//custom fields
import StatusField from "../Fields/StatusField";

const Dashboard = () => (
    <Grid container spacing={2}>
        <Grid item xs={12} lg={12}>
            <Card>
                <Title title="Placeholder Admin" />
                <CardHeader title="Welcome to the administration" />
                <CardContent>Lorem ipsum sic dolor amet...</CardContent>
            </Card>
        </Grid>
        <Grid item xs={12} lg={9}>
            <Card>
                <CardHeader title="Recent Posts" />
                <CardContent>
                <List 
                    title=" " 
                    actions={null} 
                    resource="posts" 
                    perPage={6} 
                    pagination={false} 
                    sort={{field: "id", order: "DESC"}}
                >
                    <SimpleList 
                        primaryText={record => record.title} 
                        secondaryText={ record => 
                        <ReferenceField source="userId" reference="users">
                            <TextField source="username" />
                        </ReferenceField>}
                        linkType="show"
                        />
                </List>
                </CardContent>
            </Card>
        </Grid>
        <Grid item xs={12} lg={3}>
            <Card>
                <CardHeader title="Users Status" />
                    <List title=" " actions={null} pagination={false} resource="users">
                        <SimpleList 
                            primaryText={record => <StatusField source="username"/>} 
                            linkType="show"
                            />
                    </List>
            </Card>
        </Grid>
        <Grid item xs={12} lg={12}>
            <Card>
                <CardHeader title="Unfinished tasks" />
                <CardContent>
                <List 
                title=" " 
                perPage={100} 
                actions={null} 
                resource="todos" 
                pagination={false} 
                filter={{completed: false}}
                >
                    <Datagrid actions={null}>
                        <ReferenceField reference="users" source="userId" link="show">
                            <TextField source="username"/>
                        </ReferenceField>
                        <TextField source="title" />
                        <BooleanField 
                            source="completed" 
                            TrueIcon={record => <DoneIcon sx={{color: "green"}}/>} 
                            FalseIcon={record => <ClearIcon sx={{color: "red"}}/>} 
                        />
                    </Datagrid>
                </List>
                </CardContent>
            </Card>
        </Grid>
    </Grid>
);

export default Dashboard;